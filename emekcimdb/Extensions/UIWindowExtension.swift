//
//  UIWindowExtension.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright (c) 2020 Hasan Ali Asan. All rights reserved.
//

import UIKit

extension UIWindow {
  func setRootWireFrame(wireFrame: BaseWireframe) {
    if let nav = wireFrame.navigationController {
      self.rootViewController = nav
      return
    }
    
    self.rootViewController = wireFrame.viewController
  }
}
