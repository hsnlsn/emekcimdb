//
//  UIVCExt.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright (c) 2020 Hasan Ali Asan. All rights reserved.
//

import UIKit

enum AppStoryboard: String {
  case movie = "Movie"
  case Auth = "Auth"
  
  var instance : UIStoryboard {
    return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
  }
  
  func viewController<T : UIViewController>(viewControllerClass: T.Type,
                                            function: String = #function,
                                            line: Int = #line,
                                            file: String = #file) -> T {
    let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
    guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
      fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard.\nFile : \(file)")
    }
    
    return scene
  }
  
  func initialViewController() -> UIViewController? {
    return instance.instantiateInitialViewController()
  }
  
  func initWithIdentifier(identifier: String) -> UIViewController? {
    return instance.instantiateViewController(withIdentifier: identifier)
  }
}

extension UIViewController {
  static var storyboardID : String {
    return "\(self)"
  }
  
  static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
    return appStoryboard.viewController(viewControllerClass: self)
  }
}
