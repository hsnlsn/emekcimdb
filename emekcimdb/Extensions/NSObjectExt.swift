//
//  NSObjectExt.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright (c) 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation

extension NSObject {
  static var nameAsString: String {
    return String(describing: self)
  }
}
