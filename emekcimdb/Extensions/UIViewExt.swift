//
//  UIViewExt.swift
//  emekcimdb
//
//  Created by Hasan Asan on 30.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import UIKit

extension UIView {
  /// bold = 1, boldItalic = 2, heavy = 3, heavyItalic = 4, light = 5, lightItalic = 6, medium = 7, mediumItalic = 8, regular = 9, italic = 10, semibold = 11, semiboldItalic = 12
  @IBInspectable var hDescriptorForDefaultFont: Int {
    get {
      return self.hDescriptorForDefaultFont
    }
    
    set {
      var f = HConstants.DEFAULT_FONT.regular
      switch newValue {
      case 1:
        f = HConstants.DEFAULT_FONT.bold
      case 2:
        f = HConstants.DEFAULT_FONT.boldItalic
      case 3:
        f = HConstants.DEFAULT_FONT.regular
      case 4:
        f = HConstants.DEFAULT_FONT.regular
      case 5:
        f = HConstants.DEFAULT_FONT.light
      case 6:
        f = HConstants.DEFAULT_FONT.lightItalic
      case 7:
        f = HConstants.DEFAULT_FONT.medium
      case 8:
        f = HConstants.DEFAULT_FONT.mediumItalic
      case 9:
        f = HConstants.DEFAULT_FONT.regular
      case 10:
        f = HConstants.DEFAULT_FONT.italic
      case 11:
        f = HConstants.DEFAULT_FONT.semiBold
      case 12:
        f = HConstants.DEFAULT_FONT.semiBoldItalic
      default:
        f = HConstants.DEFAULT_FONT.regular
      }
      
      if let v = self as? UILabel {
        v.font = f.font(size: v.font.pointSize)
      }
      
      if let v = self as? UITextView {
        v.font = f.font(size: v.font!.pointSize)
      }
      
      if let v = self as? UIButton, let _titleLabel = v.titleLabel {
        _titleLabel.font = f.font(size: _titleLabel.font.pointSize)
      }
    }
  }
}
