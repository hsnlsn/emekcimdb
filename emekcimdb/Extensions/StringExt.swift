//
//  StringExt.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright (c) 2020 Hasan Ali Asan. All rights reserved.
//

import DeviceKit

extension String {
  var semanticVersion: String {
    var arr = self.components(separatedBy: ".")
    if arr.count > 3 {
      arr.removeSubrange(3...)
    } else {
      let trailingZeros = [String](repeating: "0", count: 3 - arr.count)
      arr.append(contentsOf: trailingZeros)
    }
    
    let newVersion = arr.joined(separator: ".")
    return newVersion
  }
  
  static var buildNumber: String {
    return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String ?? ""
  }
  
  static var versionNumber: String {
    let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? ""
    return version
  }
  
  static var osVersion: String {
    return UIDevice.current.systemVersion
  }
  
  static var osName: String {
    return Device.current.systemName ?? ""
  }
  
  static var deviceModel: String {
    return Device.current.safeDescription
  }
  
  static var deviceType: String {
    switch UI_USER_INTERFACE_IDIOM() {
    case .phone:
      return "0"
    case .pad:
      return "1"
    case .tv:
      return "2"
    case .carPlay:
      return "3"
    case .unspecified:
      return "-1"
    default:
      return "-2"
    }
  }
  
  func copyToClipboard() {
    let clipboard = UIPasteboard.general
    clipboard.string = self
  }
}
