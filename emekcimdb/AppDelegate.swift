//
//  AppDelegate.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // logger
    HLogManager.setup()
    
    // firebase
    FirebaseApp.configure()
    
    // view entry point
    let preControlWireframe = PreControlWireframe()
    self.window?.setRootWireFrame(wireFrame: preControlWireframe)
    self.window?.makeKeyAndVisible()
    
    return true
  }
}

