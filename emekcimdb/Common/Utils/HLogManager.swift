//
//  HLogManager.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright (c) 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation
import SwiftyBeaver

let log = SwiftyBeaver.self

class HLogManager {
  static let shared = HLogManager()
  var entries = [String]()
  
  class func setup(printDocumentsPaths: Bool = false) {
    let console = ConsoleDestination()
    log.addDestination(console)
    
    if printDocumentsPaths, let documentsPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
      log.verbose("documentsPathString \(documentsPathString)")
    }
  }
  
  class func logError(error: Error) {
    log.error(error.localizedDescription)
  }
}

