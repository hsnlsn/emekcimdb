//
//  HNavigation.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright (c) 2020 Hasan Ali Asan. All rights reserved.
//

import UIKit
import SVProgressHUD

struct HNavigation {
  static func showLoader(status: String? = nil) {
    HNavigation.resignAllResponders()
    
    DispatchQueue.main.async {
      SVProgressHUD.show(withStatus: status)
    }
  }
  
  static func hideLoader() {
    DispatchQueue.main.async {
      if SVProgressHUD.isVisible() {
        SVProgressHUD.dismiss()
      }
    }
  }
  
  static func showSuccess(status: String?) {
    DispatchQueue.main.async {
      SVProgressHUD.showSuccess(withStatus: status)
      SVProgressHUD.dismiss(withDelay: 2)
    }
  }
  
  static func showError(status: String?) {
    DispatchQueue.main.async {
      SVProgressHUD.showError(withStatus: status)
      SVProgressHUD.dismiss(withDelay: 2)
    }
  }
  
  static func showSystemAlert(_ title: String?,
                              message: String?,
                              buttonTitle: String? = "Ok",
                              vc: UIViewController,
                              completion: VoidCallback = nil) -> Void {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.view.tintColor = .black
    
    let firstAction = UIAlertAction(title: buttonTitle, style: .default) { (_) in
      if completion != nil {
        completion!()
      }
    }
    
    alert.addAction(firstAction)
    DispatchQueue.main.async {
      vc.present(alert, animated: true, completion: nil)
    }
  }
  
  static func showSystemAlert(title: String?,
                              message: String?,
                              leftButton: String?,
                              rightButton: String?,
                              vc: UIViewController,
                              completionLeft: VoidCallback = nil,
                              completionRight: VoidCallback = nil) -> Void {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.view.tintColor = .black
    
    let firstAction = UIAlertAction(title: leftButton, style: .default) { (_) in
      if completionLeft != nil {
        completionLeft!()
      }
    }
    
    let secondAction = UIAlertAction(title: rightButton, style: .default) { (_) in
      if completionRight != nil {
        completionRight!()
      }
    }
    
    alert.addAction(firstAction)
    alert.addAction(secondAction)
    DispatchQueue.main.async {
      vc.present(alert, animated: true, completion: nil)
    }
  }
  
  static func presentShareActivity(content:String?,
                                   image:UIImage?,
                                   url:String?,
                                   sourceRect: CGRect?,
                                   me: UIViewController,
                                   completion: ((UIActivity.ActivityType?, Bool?, Error?) -> Void)? = nil){
    var objectsToShare = [Any]()
    
    if let content = content {
      objectsToShare.append(content)
    }
    
    if let url = url {
      objectsToShare.append(url)
    }
    
    if let image = image {
      objectsToShare.append(image)
    }
    
    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
    activityVC.modalPresentationStyle = .popover
    activityVC.popoverPresentationController?.sourceView = me.view
    if let sourceRect = sourceRect {
      activityVC.popoverPresentationController?.sourceRect = sourceRect
    }
    
    me.present(activityVC, animated: true, completion: nil)
    activityVC.completionWithItemsHandler = {(activity, success, items, error) in
      if completion != nil {
        completion!(activity, success, error)
      }
    }
  }
  
  static func resignAllResponders() {
    UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder),
                                    to   : nil,
                                    from : nil,
                                    for  : nil)
  }
  
  static func open(urlString: String?) {
    if let urlString = urlString, let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) {
      UIApplication.shared.open(url)
    }
  }
  
  static func openAppSettings() {
    HNavigation.open(urlString: UIApplication.openSettingsURLString)
  }
}
