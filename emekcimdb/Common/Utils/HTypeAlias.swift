//
//  HTypeAlias.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright (c) 2020 Hasan Ali Asan. All rights reserved.
//

import UIKit

// VoidCallback
typealias VoidCallback = (() -> ())?
