//
//  HValidator.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright (c) 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation
import Validator

enum HValidatorType: Equatable {
  case fieldRequired
  case minLength(length: Int)
  case maxLength(length: Int)
}

enum HValidationErrors: ValidationError {
  case fieldRequired(fieldName: String)
  case minLength(fieldName: String, length: Int)
  case maxLength(fieldName: String, length: Int)
  
  var message: String {
    switch self {
    case .fieldRequired(let fieldName):
      return "\(fieldName) alanı zorunludur."
    case .minLength(let fieldName, let length):
      if length == 1 {
        return "\(fieldName) alanı zorunludur."
      }
      
      return "\(fieldName) alanı en az \(length) karakter uzunluğunda olmalıdır."
    case .maxLength(let fieldName, let length):
      return "\(fieldName) alanı en fazla \(length) karakter uzunluğunda olabilir."
    }
  }
}

struct HValidator {
  static func validate(text: String?, fieldName: String, types: [HValidatorType]) -> String? {
    guard let text = text else {
      return HValidationErrors.fieldRequired(fieldName: fieldName).message
    }
    
    var rules = ValidationRuleSet<String>()
    for item in types {
      switch item {
      case .fieldRequired:
        let rule = ValidationRuleRequired<String>(error: HValidationErrors.fieldRequired(fieldName: fieldName))
        rules.add(rule: rule)
      case .minLength(let length):
        let rule = ValidationRuleLength(min: length, error: HValidationErrors.minLength(fieldName: fieldName, length: length))
        rules.add(rule: rule)
      case .maxLength(let length):
        let rule = ValidationRuleLength(max: length, error: HValidationErrors.maxLength(fieldName: fieldName, length: length))
        rules.add(rule: rule)
      }
    }
    
    switch text.validate(rules: rules) {
    case .valid:
      return nil
    case .invalid(let errors):
      return errors.first?.message
    }
  }
}


