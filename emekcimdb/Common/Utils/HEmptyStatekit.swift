//
//  HEmptyStatekit.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright (c) 2020 Hasan Ali Asan. All rights reserved.
//

import UIKit
import EmptyStateKit

enum HCustomEmptyState: CustomState {
  case noSearchResult
  case searchIdle
  
  var image: UIImage? {
    switch self {
    case .noSearchResult:
      return Asset.Images.emptyState.image
    case .searchIdle:
      return Asset.Images.searchIdleState.image
    }
  }
  
  var title: String? {
    switch self {
    case .noSearchResult:
      return "Sorry, found nothing!"
    case .searchIdle:
      return "Type something to search."
    }
  }
  
  var description: String? {
    return nil
  }
  
  var titleButton: String? {
    return nil
  }
}

extension EmptyState {
  func setCustomFormat() -> EmptyState {
    self.format.backgroundColor = .clear
    self.format.titleAttributes = [.font: HConstants.DEFAULT_FONT.medium.font(size: 14),
                                   .foregroundColor: UIColor.lightGray]
    return self
  }
  
  func show(for state: HCustomEmptyState? = nil) {
    self.setCustomFormat().show(state)
  }
}
