//
//  HAnalyticManager.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import Firebase

enum AnalyticEventName: String {
  case movieDetail = "movie_detail"
}

struct HAnalyticManager {
  static func logEvent(name: String, parameters: [String: Any]?) {
    Analytics.logEvent(name, parameters: parameters)
  }
}
