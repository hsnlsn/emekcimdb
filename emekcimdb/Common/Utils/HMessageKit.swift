//
//  HMessageKit.swift
//  emekcimdb
//
//  Created by Hasan Asan on 30.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import UIKit
import SwiftEntryKit

enum HMessageKitAlertType {
  case success
  case warning
  case error
  case info
  case free
}

enum HMessageKitAlertLayout {
  case notification
  case note
  case alert
  case tab
  case center
}

struct HMessageKit {
  static func showNotification(_ title: String?,
                               message: String?,
                               duration: Double = 3.0,
                               type: HMessageKitAlertType = .warning) {
    var alertColor: UIColor!
    var textColor: UIColor!
    switch type {
    case .error:
      alertColor = Asset.Colors.bgDanger.color
      textColor = Asset.Colors.textDanger.color
      break
    case .success:
      alertColor = Asset.Colors.bgSuccess.color
      textColor = Asset.Colors.textSuccess.color
      break
    case .info:
      alertColor = Asset.Colors.bgInfo.color
      textColor = Asset.Colors.textInfo.color
      break
    case .free:
      alertColor = .black
      textColor = .white
      break
    default:
      alertColor = Asset.Colors.bgWarning.color
      textColor = Asset.Colors.textWarning.color
      break
    }
    
    var attributes = EKAttributes.float
    attributes.displayDuration = duration
    attributes.precedence.priority = .high
    
    attributes.border = .value(color: alertColor, width: 1)
    attributes.entryBackground = .color(color: EKColor(alertColor))
    
    let animation: EKAttributes.Animation = .init(translate: .init(duration: 0.2), scale: .init(from: 1, to: 0.7, duration: 0.2))
    attributes.popBehavior = .animated(animation: animation)
    
    let translate: EKAttributes.Animation.Translate = .init(duration: 0.2, spring: .init(damping: 0.9, initialVelocity: 0))
    attributes.entranceAnimation = .init(translate: translate, scale: .none, fade: .init(from: 0.7, to: 1, duration: 0.2))
    attributes.exitAnimation = .init(translate: .init(duration: 0.2), scale: .none, fade: .init(from: 1, to: 0, duration: 0.2))
    
    let widthConstraint = EKAttributes.PositionConstraints.Edge.offset(value: 16)
    let heightConstraint = EKAttributes.PositionConstraints.Edge.intrinsic
    attributes.positionConstraints.size = .init(width: widthConstraint, height: heightConstraint)
    
    let titleLabelStyle: EKProperty.LabelStyle = .init(font: HConstants.DEFAULT_FONT.bold.font(size: 13), color: EKColor(textColor), alignment: .left)
    let titleLabel = EKProperty.LabelContent(text: title ?? "", style: titleLabelStyle)
    
    let descLabelStyle: EKProperty.LabelStyle = .init(font: HConstants.DEFAULT_FONT.semiBold.font(size: 14), color: EKColor(textColor), alignment: .left)
    let descriptionLabel = EKProperty.LabelContent(text: message ?? "", style: descLabelStyle)
    
    let simpleMessage = EKSimpleMessage(title: titleLabel, description: descriptionLabel)
    
    let titleToDescSpace: CGFloat = (title == nil || title == "") ? 0 : 5
    var insets = EKNotificationMessage.Insets.default
    insets.titleToDescription = titleToDescSpace
    
    let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage, insets: insets)
    let contentView = EKNotificationMessageView(with: notificationMessage)
    
    SwiftEntryKit.display(entry: contentView, using: attributes)
  }
}
