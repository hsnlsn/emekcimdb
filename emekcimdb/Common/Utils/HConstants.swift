//
//  Constants.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright (c) 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation

// MARK: App Constants

struct HConstants {
  static var API_BASE_URL = "http://www.omdbapi.com/"
  static var OMDB_API_KEY = "1d44d2d"
  
  static var DEFAULT_FONT = FontFamily.Poppins.self
  
  struct UserDefaults {
    static let STAY_LOGGED_IN = "StayLoggedInKey"
  }
  
  struct SegueIdentifier { }
  
  struct Translation { }
  
  struct InfoPList { }
}
