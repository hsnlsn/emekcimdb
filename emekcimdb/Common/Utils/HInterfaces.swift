//
//  HInterfaces.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright (c) 2020 Hasan Ali Asan. All rights reserved.
//

import UIKit

// MARK: HReusableView Protocol

protocol HReusableView {
  static var reuseIdentifier: String { get }
}

// MARK: HReusableView Extension

extension HReusableView {
  static var reuseIdentifier: String {
    return String(describing: self)
  }
}

// MARK: UITableViewCell Extension

extension UITableViewCell: HReusableView {}

// MARK: UICollectionViewCell Extension

extension UICollectionViewCell: HReusableView {}
