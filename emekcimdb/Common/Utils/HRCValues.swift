//
//  HRCValues.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import FirebaseRemoteConfig

typealias RemoteConfigFetchBlock = ((RemoteConfigFethcStatus) -> Void)

enum RemoteConfigFethcStatus {
  case fetched
  case failed
}

enum RemoteConfigKey: String {
  case precontrolToastMessageLoodos = "precontrol_toast_message_loodos"
}

class HRCValues {
  static let shared = HRCValues()
  
  init() {
    self.loadDefaults()
  }
  
  func loadDefaults() {
    let appDefaults: [String: Any?] = [
      "precontrol_toast_message_loodos" : "Loodos(Default)"
    ]
    
    RemoteConfig.remoteConfig().setDefaults(appDefaults as? [String: NSObject])
  }
  
  func getRemoteValues(completion: @escaping RemoteConfigFetchBlock) {
    let fetchDuration: TimeInterval = 0
    RemoteConfig.remoteConfig().fetch(withExpirationDuration: fetchDuration) { status, error in
      if status == .failure {
        completion(.failed)
        return
      }
      
      // activate new values
      RemoteConfig.remoteConfig().activate(completion: nil)
      
      //
      completion(.fetched)
    }
  }
  
  static func getString(for key: RemoteConfigKey) -> String? {
    return RemoteConfig.remoteConfig().configValue(forKey: key.rawValue).stringValue
  }
}
