//
//  MovieEndPoint.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation

public enum MovieEndPoint {
  case searchMovie(urlParameters: Parameters?)
}

extension MovieEndPoint: HEndpointType {
  var path: String {
    switch self {
    case .searchMovie:
      return "/"
    }
  }
  
  var httpMethod: HTTPMethod {
    switch self {
    case .searchMovie:
      return .get
    }
  }
  
  var task: HTTPTask {
    switch self {
    case .searchMovie(let urlParameters):
      return .requestParametersAndHeaders(bodyEncoding: .urlEncoding,
                                          bodyParameters: nil,
                                          urlParameters: urlParameters)
    }
  }
}


