//
//  ParameterEncoding.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation

public typealias Parameters = [String: Any]
public typealias MultipartParameterData = [String: Data]

public protocol ParameterEncoder {
  func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws
}

public indirect enum ParameterEncoding {
  
  case urlEncoding
  case jsonEncoding
  case urlAndJsonEncoding(ParameterEncoding)
  case multipartEncoding
  
  public func encode(urlRequest: inout URLRequest,
                     bodyParameters: Parameters?,
                     urlParameters: Parameters?,
                     pathParameters: Parameters?) throws {
    do {
      if let pathParameters = pathParameters {
        try PATHParameterEncoding().encode(urlRequest: &urlRequest, with: pathParameters)
      }
      
      if let urlParameters = urlParameters { 
        try URLParameterEncoder().encode(urlRequest: &urlRequest, with: urlParameters)
      }
      
      if let bodyParameters = bodyParameters {
        try JSONParameterEncoder().encode(urlRequest: &urlRequest, with: bodyParameters)
      }
      
      switch self {
      case .urlEncoding:
        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
          urlRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        }
      case .jsonEncoding, .urlAndJsonEncoding:
        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
          urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
      default:
        break
      }
    } catch {
      throw error
    }
  }
}

public enum NetworkError : String, Error {
  case parametersNil = "Parameters were nil."
  case encodingFailed = "Parameter encoding failed."
  case missingURL = "URL is nil."
}

