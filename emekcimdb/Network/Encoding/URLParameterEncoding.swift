//
//  URLEncoding.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation

public struct URLParameterEncoder: ParameterEncoder {
  public func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws {
    
    guard let url = urlRequest.url else { throw NetworkError.missingURL }
    if var urlComponents = URLComponents(url: url,
                                         resolvingAgainstBaseURL: false), !parameters.isEmpty {
      
      urlComponents.queryItems = [URLQueryItem]()
      
      for (key,value) in parameters {
        let queryItem = URLQueryItem(name: key,
                                     value: "\(value)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed))
        urlComponents.queryItems?.append(queryItem)
      }
      urlRequest.url = urlComponents.url
    }
  }
}
