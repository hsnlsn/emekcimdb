//
//  PATHParameterEncoding.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation

public struct PATHParameterEncoding: ParameterEncoder {
  public func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws {
    guard let url = urlRequest.url else { throw NetworkError.missingURL }
    
    if let decodedUrlStr = url.absoluteString.removingPercentEncoding {
      let newUrlStr = decodedUrlStr.replacePathParameters(pathParameters: parameters)
      
      if let encodedStr = newUrlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
        urlRequest.url = URL(string: encodedStr)
      }
    }
  }
}

fileprivate extension String {
  func replacePathParameters(pathParameters: Parameters?) -> String {
    var pathString = self
    pathParameters?.keys.forEach({ (key) in
      if let val = pathParameters?[key] as? String {
        pathString = pathString.replacingOccurrences(of: "{{\(key)}}", with: val)
      }
    })
    
    return pathString
  }
}
