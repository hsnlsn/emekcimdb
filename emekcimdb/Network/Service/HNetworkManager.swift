//
//  HNetworkManager.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation
import Alamofire
import SDWebImage
import ObjectMapper

enum HNetworkEnvironment {
  case development
  case production
}

enum HNetworkResult<T> {
  case success(T)
  case failure(HNetworkError)
}

typealias ProgressBlock = ((_ progress: Progress?) -> Void)?
typealias RequestCompletionBlock<T> = ((HNetworkResult<T>) -> Void)?
typealias ImageCompletionBlock = ((UIImage?) -> Void)

struct HNetworkManager {
  static let shared = HNetworkManager()
  
  static func sendRequest<T: Mappable>(with endpoint: HEndpointType,
                                       objectType: T.Type,
                                       completion: RequestCompletionBlock<T>) {
    do {
      let request = try self.buildRequest(from: endpoint)
      AF.request(request)
        .responseJSON { responseJson in
          switch responseJson.result {
          case .success(let value):
            if let val = value as? [String: Any], let decodedObject = objectType.init(JSON: val) {
              completion?(.success(decodedObject))
            } else {
              completion?(.failure(HNetworkError.decodingFailed))
            }
          case .failure(_):
            completion?(.failure(.failed))
          }
      }
    } catch {
      completion?(.failure(.badRequest))
    }
  }
  
  static func sendMultipartRequest<T: Mappable>(with endpoint: HEndpointType,
                                                multipartParameters: MultipartParameterData,
                                                objectType: T.Type,
                                                progressCallback: ProgressBlock? = nil,
                                                completion: RequestCompletionBlock<T>) {
    var headers: Alamofire.HTTPHeaders = [:]
    endpoint.commonHeaders?.forEach({ (key, value) in
      headers[key] = value
    })
    
    endpoint.additionalHeaders?.forEach({ (key, value) in
      headers[key] = value
    })
    
    let urlString = endpoint.baseURL.appendingPathComponent(endpoint.path)
    AF.upload(multipartFormData: { multipartFormData in
      multipartParameters.forEach { (key, value) in
        multipartFormData.append(value, withName: key, fileName: "file.jpg", mimeType: "image/jpeg")
      }
    }, to: urlString,
       method: .post,
       headers: headers)
      .uploadProgress(closure: { (progress) in
        if progressCallback != nil {
          progressCallback!!(progress)
        }
      })
      .responseJSON { responseJson in
        switch responseJson.result {
        case .success(let value):
          if let val = value as? [String: Any], let decodedObject = objectType.init(JSON: val) {
            completion?(.success(decodedObject))
          } else {
            completion?(.failure(HNetworkError.decodingFailed))
          }
        case .failure(_):
          completion?(.failure(.failed))
        }
    }
  }
  
  static func downloadImage(urlString: String?,
                            completion: @escaping ImageCompletionBlock) {
    guard let _urlString = urlString else {
      completion(nil)
      return
    }
    
    guard let url = URL(string: _urlString) else {
      completion(nil)
      return
    }
    
    SDWebImageManager.shared().loadImage(with: url,
                                         options: [],
                                         progress: nil)
    { (image, data, error, cacheType, finished, url) in
      completion(image)
    }
  }
  
  static func clearCacheData() {
    URLCache.shared.removeAllCachedResponses()
  }
}

extension HNetworkManager {
  fileprivate static func buildRequest(from endpoint: HEndpointType) throws -> URLRequest {
    let cachePolicy = HConnectivity.isConnectedToInternet() ? endpoint.cachePolicy : .returnCacheDataDontLoad
    var request = URLRequest(url: endpoint.baseURL.appendingPathComponent(endpoint.path),
                             cachePolicy: cachePolicy,
                             timeoutInterval: endpoint.timeoutInterval)
    
    request.httpMethod = endpoint.httpMethod.rawValue
    
    if let headers = endpoint.commonHeaders {
      for key in headers.keys {
        request.setValue(headers[key], forHTTPHeaderField: key)
      }
    }
    
    do {
      switch endpoint.task {
      case .requestParametersAndHeaders(let bodyEncoding,
                                        let bodyParameters,
                                        let urlParameters,
                                        let pathParameters,
                                        let additionalHeaders):
        self.addAdditionalHeaders(additionalHeaders, request: &request)
        try self.configureParameters(bodyParameters: bodyParameters,
                                     bodyEncoding: bodyEncoding,
                                     urlParameters: urlParameters,
                                     pathParameters: pathParameters,
                                     request: &request)
      }
      
      return request
    } catch {
      throw error
    }
  }
  
  fileprivate static func configureParameters(bodyParameters: Parameters?,
                                              bodyEncoding: ParameterEncoding,
                                              urlParameters: Parameters?,
                                              pathParameters: Parameters?,
                                              request: inout URLRequest) throws {
    do {
      try bodyEncoding.encode(urlRequest: &request,
                              bodyParameters: bodyParameters,
                              urlParameters: urlParameters,
                              pathParameters: pathParameters)
    } catch {
      throw error
    }
  }
  
  fileprivate static func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
    guard let headers = additionalHeaders else { return }
    for (key, value) in headers {
      request.setValue(value, forHTTPHeaderField: key)
    }
  }
}

class HConnectivity {
  class func isConnectedToInternet() -> Bool {
    return NetworkReachabilityManager()?.isReachable ?? false
  }
}
