//
//  EndPoint.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation

protocol HEndpointType {
  var baseURL: URL { get }
  var path: String { get }
  var httpMethod: HTTPMethod { get }
  var task: HTTPTask { get }
  var commonHeaders: HTTPHeaders? { get }
  var authRequired: Bool? { get }
  var cachePolicy: NSURLRequest.CachePolicy { get }
  var timeoutInterval: TimeInterval { get }
  var additionalHeaders: HTTPHeaders? { get }
}

extension HEndpointType {
  var baseURL: URL {
    guard let url = URL(string: HConstants.API_BASE_URL) else { fatalError("baseURL could not be configured.")}
    return url
  }
  
  var authRequired: Bool? {
    return true
  }
  
  var timeoutInterval: TimeInterval {
    return 20
  }
  
  var commonHeaders: HTTPHeaders? {
    return ["x-os-version": String.osVersion,
            "x-os-name": String.osName,
            "x-device-model": String.deviceModel,
            "x-device-type": String.deviceType,
            "x-app-version": String.versionNumber,
            "x-build-number": String.buildNumber,
            "x-timezone": TimeZone.current.description]
  }
  
  var additionalHeaders: HTTPHeaders? {
    return nil
  }
  
  var cachePolicy: NSURLRequest.CachePolicy {
    return .reloadIgnoringLocalCacheData
  }
}

