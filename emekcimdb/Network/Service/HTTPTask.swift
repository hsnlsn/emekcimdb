//
//  HTTPTask.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation

public typealias HTTPHeaders = [String: String]

public enum HTTPTask {
  case requestParametersAndHeaders(
    bodyEncoding: ParameterEncoding,
    bodyParameters: Parameters?,
    urlParameters: Parameters?,
    pathParameters: Parameters? = nil,
    additionHeaders: HTTPHeaders? = nil)
}


