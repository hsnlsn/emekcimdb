//
//  HTTPMethod.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation

public enum HTTPMethod : String {
  case get     = "GET"
  case post    = "POST"
  case put     = "PUT"
  case patch   = "PATCH"
  case delete  = "DELETE"
}
