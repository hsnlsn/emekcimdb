//
//  HNetworkResponse.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation

struct HNetworkResponse {
  static func handleNetworkResponse(for response: HTTPURLResponse?) -> HNetworkResult<String> {
    guard let res = response else { return HNetworkResult.failure(HNetworkError.UnwrappingError)}
    
    switch res.statusCode {
    case 200...299: return HNetworkResult.success("")
    case 401: return HNetworkResult.failure(HNetworkError.authenticationError)
    case 400...499: return HNetworkResult.failure(HNetworkError.badRequest)
    case 503: return HNetworkResult.failure(HNetworkError.serviceUnavailable)
    case 500...599: return HNetworkResult.failure(HNetworkError.serverSideError)
    default: return HNetworkResult.failure(HNetworkError.failed)
    }
  }
}
