//
//  ApiError.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import Foundation

enum HNetworkError: Error {
  case dataNotFound
  case malformedURL
  case parametersNil
  case encodingFailed
  case decodingFailed
  case serviceUnavailable
  case headersNil
  case couldNotParse
  case noData
  case FragmentResponse
  case UnwrappingError
  case dataTaskFailed
  case authenticationError
  case badRequest
  case pageNotFound
  case failed
  case serverSideError
}

extension HNetworkError: LocalizedError {
  var errorDescription: String? {
    switch self {
    case .dataNotFound:
      return NSLocalizedString("Data not found!", comment: "")
    case .malformedURL:
      return NSLocalizedString("Malformed URL!", comment: "")
    case .parametersNil:
      return NSLocalizedString("Parameters were nil.", comment: "")
    case .encodingFailed:
      return NSLocalizedString("Parameter encoding failed.", comment: "")
    case .decodingFailed:
      return NSLocalizedString("Response deencoding failed.", comment: "")
    case .serviceUnavailable:
      return NSLocalizedString("Service Unavailable.", comment: "")
    case .headersNil:
      return NSLocalizedString("Headers are Nil", comment: "")
    case .couldNotParse:
      return NSLocalizedString("Unable to parse the JSON response.", comment: "")
    case .noData:
      return NSLocalizedString("The data from API is Nil.", comment: "")
    case .FragmentResponse:
      return NSLocalizedString("The API's response's body has fragments.", comment: "")
    case .UnwrappingError:
      return NSLocalizedString("Unable to unwrape the data.", comment: "")
    case .dataTaskFailed:
      return NSLocalizedString("The Data Task object failed.", comment: "")
    case .authenticationError:
      return NSLocalizedString("You must be Authenticated", comment: "")
    case .badRequest:
      return NSLocalizedString("Bad Request", comment: "")
    case .pageNotFound:
      return NSLocalizedString("Page/Route rquested not found.", comment: "")
    case .failed:
      return NSLocalizedString("Network Request failed", comment: "")
    case .serverSideError:
      return NSLocalizedString("Server error", comment: "")
    }
  }
}
