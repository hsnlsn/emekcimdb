//
//  PreControlPresenter.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright (c) 2020 Hasan Ali Asan. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import Foundation

final class PreControlPresenter {
  
  // MARK: - Private properties -
  
  private unowned let view: PreControlViewInterface
  private let interactor: PreControlInteractorInterface
  private let wireframe: PreControlWireframeInterface
  
  private var timer: Timer?
  private var currentCount = 0
  private static let GOING_HOME_SECONDS = 4
  
  // MARK: - Lifecycle -
  
  init(view: PreControlViewInterface,
       interactor: PreControlInteractorInterface,
       wireframe: PreControlWireframeInterface) {
    self.view = view
    self.interactor = interactor
    self.wireframe = wireframe
    
    self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
      self.currentCount += 1
      
      self.showTimerMessage()
      
      if self.currentCount == PreControlPresenter.GOING_HOME_SECONDS {
        timer.invalidate()
        
        self.goHome()
      }
    })
  }
  
  func showTimerMessage() {
    self.view.showTimerMessage(text: "Going home in \(PreControlPresenter.GOING_HOME_SECONDS - self.currentCount)...")
  }
}

// MARK: - Extensions -

extension PreControlPresenter: PreControlPresenterInterface {
  func viewDidAppear(animated: Bool) {
    if !self.interactor.isConnectedToInternet() {
      self.view.showInternetIsUnreachableAlert()
      return
    }
    
    // remote config
    self.view.showWelcomeMessage(text: "Fethcing Remote Config Data...")
    self.interactor.getRemoteConfigValues { (status) in
      switch status {
      case .failed:
        self.view.showWelcomeMessage(text: "Could not fetch data!")
      case .fetched:
        self.view.showWelcomeMessage(text: HRCValues.getString(for: .precontrolToastMessageLoodos))
        
        self.timer?.fire()
      }
    }
  }
  
  func goHome() {
    DispatchQueue.main.async {
      self.wireframe.navigate(to: .home)
    }
  }
}
