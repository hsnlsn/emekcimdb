//
//  MovieRating.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import ObjectMapper

class MovieRating: Mappable {
  var source: String?
  var value: String?
  
  required init?(map: Map) { }
  
  func mapping(map: Map) {
    self.source <- map["Source"]
    self.value <- map["Value"]
  }
}
