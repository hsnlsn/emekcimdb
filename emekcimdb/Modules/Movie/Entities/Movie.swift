//
//  Movie.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import ObjectMapper

enum SearchResponseType: String {
  case success = "true"
  case failure = "false"
}

class Movie: Mappable {
  var title: String?
  var year: String?
  var rated: String?
  var released: String?
  var runtime: String?
  var genre: String?
  var director: String?
  var writer: String?
  var actors: String?
  var plot: String?
  var language: String?
  var country: String?
  var awards: String?
  var poster: URL?
  var metascore: String?
  var imdbRating: String?
  var imdbVotes: String?
  var imdbID: String?
  var type: String?
  var dvd: String?
  var boxOffice: String?
  var production: String?
  var error: String?
  var website: URL?
  
  var ratings = [MovieRating]()
  var response = SearchResponseType.success
  
  required init?(map: Map) { }
  
  func mapping(map: Map) {
    self.title <- map["Title"]
    self.year <- map["Year"]
    self.rated <- map["Rated"]
    self.released <- map["Released"]
    self.runtime <- map["Runtime"]
    self.genre <- map["Genre"]
    self.director <- map["Director"]
    self.writer <- map["Writer"]
    self.actors <- map["Actors"]
    self.plot <- map["Plot"]
    self.language <- map["Language"]
    self.country <- map["Country"]
    self.awards <- map["Awards"]
    self.poster <- (map["Poster"], URLTransform())
    self.metascore <- map["Metascore"]
    self.imdbRating <- map["imdbRating"]
    self.imdbVotes <- map["imdbVotes"]
    self.imdbID <- map["imdbID"]
    self.type <- map["Type"]
    self.dvd <- map["Dvd"]
    self.boxOffice <- map["BoxOffice"]
    self.production <- map["Production"]
    self.error <- map["Error"]
    self.website <- (map["Website"], URLTransform())
    self.response <- (map["Response"], EnumTransform<SearchResponseType>())
  }
}

extension Movie: MovieSearchResultItemWireframe {
  var _title: String? {
    return self.title
  }
  
  var _imdbRating: String? {
    return "IMDB: \(self.imdbRating ?? "")"
  }
  
  var _duration: String? {
    return self.runtime
  }
  
  var _genre: String? {
    return self.genre
  }
  
  var _posterURL: URL? {
    return self.poster
  }
}

extension Movie: MovieDetailItemWireframe {
  var _writer: String? {
    return self.writer
  }
  
  var _director: String? {
    return self.director
  }
  
  var _actors: String? {
    return self.actors
  }
  
  var _country: String? {
    return self.country
  }
  
  var _releaseDate: String? {
    return self.released
  }
  
  var _year: String? {
    return self.year
  }
  
  var _imdbVotes: String? {
    return self.imdbVotes
  }
  
  var _language: String? {
    return self.language
  }
  
  var _plot: String? {
    return self.plot
  }
}
