//
//  SearchResultTVCell.swift
//  emekcimdb
//
//  Created by Hasan Asan on 29.08.2020.
//  Copyright © 2020 Hasan Ali Asan. All rights reserved.
//

import UIKit

class SearchResultTVCell: UITableViewCell {
  @IBOutlet weak var movieTitleLabel: UILabel!
  @IBOutlet weak var movieIMDBRatingLabel: UILabel!
  @IBOutlet weak var movieGenreLabel: UILabel!
  @IBOutlet weak var movieDurationLabel: UILabel!
  @IBOutlet weak var moviePosterImageView: UIImageView! {
    didSet {
      self.moviePosterImageView.layer.cornerRadius = 6
      self.moviePosterImageView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
      self.moviePosterImageView.layer.borderWidth = 1
    }
  }
  
  func configure(with item: MovieSearchResultItemWireframe) {
    self.movieTitleLabel.text = item._title
    self.movieIMDBRatingLabel.text = item._imdbRating
    self.movieGenreLabel.text = item._genre
    self.movieDurationLabel.text = item._duration
    
    self.moviePosterImageView.sd_setImage(with: item._posterURL, completed: nil)
  }
}
